import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import Application from './app/index.js';


AppRegistry.registerComponent(appName, () => Application);