
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LoginScreen from './screens/Login';

import HomeScreen from './screens/Home';
import SplashScreen from './screens/Splash';

import RegisterScreen from './screens/Register';

const MainNavigator = createStackNavigator({

  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: {
      headerShown: false
    }
  },
  RegisterScreen: {
    screen: RegisterScreen,
    navigationOptions: {
      headerShown: false
    }
  },
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      headerShown: false
    }
  }
});


const Application = createAppContainer(createSwitchNavigator(
  {

    SplashScreen: {
      screen: SplashScreen,
      navigationOptions: {
        header: null
      }
    },
    Auth: MainNavigator
  },
  {
    initialRouteName: 'SplashScreen',
    backBehavior: 'App'
  }
));


export default Application;