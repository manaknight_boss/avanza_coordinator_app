import React from "react";
import { button, View, Text, TouchableOpacity, Keyboard, Button, TextInput, Image, SafeAreaView } from "react-native"
import commonStyles from "../../asserts/style"
import styles from "./style"
import strings from "../../asserts/strings";
import AppLoaderLessOpacity from "../../components/ActivityIndicatorLessOpacity";
import { Dialog } from 'react-native-simple-dialogs';
import colors from "../../asserts/colors";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default class RegisterScreen extends React.Component {

    state = {
        id: '',
        password: '',
        loading: false,
        modalVisible: false,
        message: '',
        titleMessage:'',
        isError:false
        
    }

    handleId = (text) => {
        this.setState({ id: text })
    }
    showModal = (message,titleMessage) => {
        this.setState({  message,titleMessage });
        this.setState({ modalVisible: true });
    }
   
    login = (id) => {
        if (id == '') {
            this.showModal(strings.idRequired,strings.error);
            this.setState({ isError: true });

            return
        }
        Keyboard.dismiss();
        this.setState({ isError: false });
        this.showModal(strings.registerMessage,strings.register)
    }
    dialogButton ()
    {
        this.setState({ modalVisible: false });
        
        if(!this.state.isError)
        {
            this.props.navigation.goBack();

        }
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <KeyboardAwareScrollView contentContainerStyle={styles.containerInside}>
                <View >
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Image
                            source={require('../../asserts/images/account.png')}
                            style={styles.margin}
                        />
                    </View>

                    <Text style={[styles.margin, styles.textCenter]}>{strings.registerTitle}</Text>
                    <View style={styles.margin}>
                        <Text style={{ padding: 5 }}>{strings.user}</Text>
                        <TextInput
                            style={styles.textField}
                            onChangeText={this.handleId}
                            autoCompleteType="off"
                            keyboardType="number-pad"
                            value={this.state.id}
                        />
                    </View>
                    

                    <TouchableOpacity style={[commonStyles.primaryButton, styles.margin]}
                        onPress={() => this.login(this.state.id)}>
                        <Text style={[commonStyles.primaryButtonText]}>{strings.register}</Text>
                    </TouchableOpacity>
                </View>
                </KeyboardAwareScrollView>
                <Dialog
                    title={this.state.titleMessage}
                    color={colors.black}
                    titleStyle={commonStyles.bold}
                    animationType="fade"
                    contentStyle={
                        {
                            alignItems: "center",
                            justifyContent: "center",
                        }
                    }
                    onTouchOutside={() =>  this.dialogButton()}
                    visible={this.state.modalVisible}
                >
                    <View>
                        <Text style={styles.dialogStyle}>{this.state.message}</Text>
                    </View>
                    <View style={styles.close}>
                        <Button onPress={() => this.dialogButton()}
                            title={strings.close}
                            color={colors.black} />

                    </View></Dialog>
                {this.state.loading ? <AppLoaderLessOpacity /> : null}
                
            </SafeAreaView >
        );
    }
}
