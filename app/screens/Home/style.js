import { StyleSheet } from 'react-native'
import colors from '../../asserts/colors';

var styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerInside: {
        flex: 1,
        flexDirection: 'column',
        alignContent: 'center',
        width: '80%',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    margin: {
        marginBottom: 14,
        marginTop: 14,
        textAlign: 'center'
    },
    textCenter: {
        textAlign: 'center',
        width: '100%'
    },
    cell: {
        textAlign: 'center',
        fontSize: 14 
    },
    header: { 
        backgroundColor: 'orange', 
        alignItems: 'center',
        flexDirection: 'column',
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        paddingTop: 60,
        width:"100%"
    },
    closeIcon:{
        position:'absolute',
        width:64,
        zIndex:9999,
        left:15,
        top:15
    },
   
})

export default styles