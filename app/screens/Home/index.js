import React from "react";
import RNExitApp from 'react-native-exit-app';

import { Text, FlatList, View, SafeAreaView, Platform, Image, Linking, TouchableOpacity, Alert, Dimensions } from "react-native"
import styles from "./style"
import { Col, Row, Grid } from "react-native-easy-grid";
import strings from "../../asserts/strings";
import AppLoader from "../../components/ActivityIndicator";
import { RecyclerListView, LayoutProvider, DataProvider } from "recyclerlistview";


export default class HomeScreen extends React.Component {

  dataProvider = new DataProvider((r1, r2) => {
    return r1 !== r2;
  });

  state = {
    id: '',
    loading: true,
    firstName: '',
    lastName: '',
    total: 0,
    voted: 0,
    notVoted: 0,
    usersList: [],
    // dataProvider: dataProvider.cloneWithRows([])
  }


  constructor(props) {
    super(props);
    let { width } = Dimensions.get("window");
    this._layoutProvider = new LayoutProvider(
      index => {
        return 0;
      },
      (type, dim) => {
        dim.width = width;
        dim.height = 140;
      }
    );




    this._rowRenderer = this._rowRenderer.bind(this);
  }

  componentDidMount() {
    const id = this.props.navigation.state.params.id;
    // const id = '00104131057';
    var baseUrl = strings.baseUrlAndroid;
    if (Platform.OS === 'ios') {
      baseUrl = strings.baseUrl
    }
    const url = baseUrl + 'v1/api/cordinator/dashboard/' + id;
    console.log("Api == " + url);
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      if (response.status == 200) {
        return response.text()
      } else {
        return "" + response.status
      }

    }).then((text) => {
      this.setState({ loading: false });
      console.log(text);
      const res = JSON.parse(text);
      console.log("data", res.data);

      this.setState({ usersList: res.data.people });
      this.setState({ firstName: res.data.summary.first_name });
      this.setState({ id: res.data.summary.government_id });
      this.setState({ lastName: res.data.summary.last_name });
      this.setState({ total: res.data.summary.total });
      this.setState({ voted: res.data.summary.voted });
      this.setState({ notVoted: res.data.summary.not_voted });
    }).catch((error) => {
      console.error(error);
    });
  }


  dialCall = (number) => {
    console.log("number", number);
    let phoneNumber = '';
    if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
    else { phoneNumber = `telprompt:${number}`; }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  };

  _rowRenderer(type, data) {

    <Row size={1} style={[{ alignItems: 'center', padding: 10 }, index % 2 == 0 ? { backgroundColor: 'white' } : { backgroundColor: '#e9ebee' }]}>
      <Col>
        <Text style={styles.cell}>{data.first_name}</Text>
      </Col>
      <Col>
        <Text style={styles.cell}>{data.last_name}</Text>
      </Col>
      <Col><View style={{ flex: 1, flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
        {data.phone_1 != "" ?
          <TouchableOpacity onPress={() => this.dialCall(data.phone_1)}><Image source={require('../../asserts/images/smartphone.png')}
            style={{ width: 20, height: 30 }}
          /></TouchableOpacity> : null}
        <Text>{' '}</Text>
        {data.phone_2 != "" ?
          <TouchableOpacity onPress={() => this.dialCall(data.phone_2)}><Image source={require('../../asserts/images/smartphone.png')}
            style={{ width: 20, height: 30 }}
          /></TouchableOpacity> : null}
      </View>
      </Col>
    </Row>

  }

  renderRow(data, index) {
    let lastName = data.last_name.replace(" ", "\n");
    return (
      <Row size={1} style={[{ alignItems: 'center', padding: 10 }, index % 2 == 0 ? { backgroundColor: 'white' } : { backgroundColor: '#e9ebee' }]} key={data.phone_1}>
        <Col>
          <Text style={styles.cell}>{data.first_name}</Text>
        </Col>
        <Col>
          <Text style={styles.cell}>{lastName}</Text>
        </Col>
        <Col><View style={{ flex: 1, flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
          {data.phone_1 != "" ?
            <TouchableOpacity onPress={() => this.dialCall(data.phone_1)}><Image source={require('../../asserts/images/smartphone.png')}
              style={{ width: 20, height: 30 }}
            /></TouchableOpacity> : null}
          <Text>{' '}</Text>
          {data.phone_2 != "" ?
            <TouchableOpacity onPress={() => this.dialCall(data.phone_2)}><Image source={require('../../asserts/images/smartphone.png')}
              style={{ width: 20, height: 30 }}
            /></TouchableOpacity> : null}
        </View>
        </Col>
      </Row>
    );
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loading ? <AppLoader /> :


          <Grid>
            <TouchableOpacity style={styles.closeIcon} onPress={() => {
              RNExitApp.exitApp();
            }}>
              <Image source={require("../../asserts/images/error.png")} />
            </TouchableOpacity>
            <Row style={styles.header}>
              <Row style={{ flex: 1, justifyContent: "center", alignItems: "center" }} size={2}>
                <Text style={[styles.textCenter, { fontSize: 17, color: 'white', fontWeight: 'bold' }]}>{this.state.firstName}{' '}{this.state.lastName}</Text>
              </Row>
              <Row style={{ flex: 1, justifyContent: "center", alignItems: "center" }} size={1}>
                <Text style={[styles.textCenter, { fontSize: 15, color: 'white', fontWeight: 'bold' }]}>{this.state.id}</Text>
              </Row>
            </Row>
            <Row size={2} style={{ backgroundColor: 'orange' }}>
              <Col>
                <Row style={{ alignItems: 'flex-end' }}>
                  <Text style={[styles.textCenter]}>{strings.subscribed}</Text>
                </Row>
                <Row>
                  <Text style={[styles.textCenter, { fontSize: 17, color: 'white', fontWeight: 'bold' }]}>{this.state.total}</Text>
                </Row>
              </Col>
              <Col>
                <Row style={{ alignItems: 'flex-end' }}>
                  <Text style={[styles.textCenter]}>{strings.voted}</Text>
                </Row>
                <Row>
                  <Text style={[styles.textCenter, { fontSize: 17, color: 'white', fontWeight: 'bold' }]}>{this.state.voted}</Text>
                </Row>
              </Col>
              <Col>
                <Row style={{ alignItems: 'flex-end' }}>
                  <Text style={[styles.textCenter]}>{strings.notVoted}</Text>
                </Row>
                <Row>
                  <Text style={[styles.textCenter, { fontSize: 17, color: 'white', fontWeight: 'bold' }]}>{this.state.notVoted}</Text>
                </Row>
              </Col>
            </Row>

            <Row style={{ backgroundColor: '#000000', alignItems: 'center' }} size={1}>

              <Text style={[styles.textCenter, { color: 'white', fontSize: 16 }]}>{strings.notVoted}</Text>

            </Row>

            <Col size={8} >
              <Row size={1} style={{ backgroundColor: '#d3d3d3', alignItems: 'center' }}>
                <Col>
                  <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold' }}>{strings.voter}</Text>
                </Col>
                <Col>
                  <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold' }}>{strings.recinto}</Text>
                </Col>
                <Col>
                  <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold' }}>{strings.action}</Text>
                </Col>
              </Row>

              <Col size={8}>
                <FlatList
                  data={this.state.usersList}
                  renderItem={({ item, index }) => this.renderRow(item, index)}
                  keyExtractor={item => item.id}
                  removeClippedSubviews={true}
                  maxToRenderPerBatch={this.state.usersList.length}
                />
                {/* <RecyclerListView
                layoutProvider={this._layoutProvider}
                dataProvider={this.state.dataProvider}
                rowRenderer={this._rowRenderer} /> */}

              </Col>
            </Col>
          </Grid>}
      </SafeAreaView>

    );
  }
}
