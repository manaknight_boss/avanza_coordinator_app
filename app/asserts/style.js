import { StyleSheet } from 'react-native'
import colors from './colors';


const commonStyles = StyleSheet.create({
  primaryButton: {
    alignSelf: 'stretch',
    marginStart: 17,
    marginEnd: 17,
    marginBottom:10,
    backgroundColor: colors.button,
    borderRadius: 10,
    paddingBottom: 18,
    paddingTop: 18
  },
  primaryButtonText: {
    fontSize: 16,
    color: colors.black,
    width:'100%',
    fontWeight: 'bold',
    textAlign:'center',
    
  }

})

export default commonStyles
