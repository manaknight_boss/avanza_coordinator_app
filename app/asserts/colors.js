
const colors = {
    button: '#FFA500',
    white: '#ffffff',
    black: '#000000',
    darkGrey: "#e8e8e8",
    primary: '#FFA500',
    errorColor: '#d50000',
    lightGrey:'#ededed',
    grey:'#c5c8cc'

}

export default colors