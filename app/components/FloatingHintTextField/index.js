import React, { Component } from 'react';
import {TextField} from 'react-native-material-textfield'
import styles from './style'
import colors from '../../asserts/colors';

class 
FloatingHintTextField extends Component {
    render() {
        return (
            <TextField TextInput secureTextEntry={this.props.secured} keyboardType ={this.props.keyboardType}
                style={styles.textInput}
                onChangeText={this.props.onChange}
                label={this.props.name}
                placeholder={this.props.text}
                error={this.props.errorMessage}
                errorColor={colors.errorColor}
                tintColor={colors.primary}
                value={this.props.value}
            />
        );
    }
}

export default FloatingHintTextField