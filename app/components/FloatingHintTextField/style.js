import { StyleSheet } from 'react-native'
import colors from '../../asserts/colors';

var styles = StyleSheet.create({

labelInput: {
    color: colors.black
},

textInput: {
    
},

input: {
    borderWidth: 0,
    paddingBottom: 0,
    paddingTop: 0
}

})
 
export default styles