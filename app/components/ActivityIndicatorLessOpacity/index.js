import React, { Component } from 'react'
import { ActivityIndicator, View, TouchableWithoutFeedback } from 'react-native'
import styles from './style'

export default class AppLoaderLessOpacity extends Component {
  render() {
    return (
      <TouchableWithoutFeedback onPress={console.log('')}>
        <View style={styles.container}>
          <ActivityIndicator size="large" />
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

